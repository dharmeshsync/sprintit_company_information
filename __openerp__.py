# -*- coding: utf-8 -*-
##############################################################################
#
#    ODOO Open Source Management Solution
#
#    ODOO Addon module by Sprintit Ltd
#    Copyright (C) 2015 Sprintit Ltd (<http://sprintit.fi>).
#
##############################################################################


{
    'name': 'Finnish Company Information',
    'version': '0.2',
    'category': 'General',
    'license': 'Other proprietary',    
    'description': 'Company information additional fields, standard for Finnish companies',
    'author': 'SprintIT, Roy Nurmi',
    'maintainer': 'SprintIT, Roy Nurmi',
    'website': 'http://www.sprintit.fi',
    'depends': [
      'base',
    ],
    'data': [
      'view/res_partner.xml',
    ],
    'demo': [
    ],
    'test': [
    ],
    'installable': True,
    'auto_install': False,
 }

